require('dotenv').config()
const Discord = require('discord.js')
const { loadCommands } = require('./utils/loader.js')
const { handleCommands } = require('./utils/handler.js')

const client = new Discord.Client()
client.commands = new Discord.Collection()
client.login(process.env.BOT_TOKEN)

client.mongoose = require('./utils/mongoose.js')
client.express = require('./utils/express.js')
client.mongoose.init()
client.express.init()

loadCommands(client)
client.on('message', message => {
	handleCommands(client, message)
})