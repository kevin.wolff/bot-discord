const { readdirSync } = require('fs')

// Get every Javascript file in ./commands/<folder>/<subfolder>/
// For each require the file.
const loadCommands = (client, dir = './commands/') => {
    readdirSync(dir).forEach(folder => {
        const folders = readdirSync(`${ dir }/${ folder }`)

        for (const subFolder of folders) {
            console.log(`Commande chargée : ${ subFolder }`)
            const subFolders = readdirSync(`${ dir }/${ folder }/${ subFolder }`).filter(files => files.endsWith('.js'))

            for (const file of subFolders) {
                const command = require(`../${ dir }/${ folder }/${ subFolder }/${ file }`)
                client.commands.set(command.info.name, command)
            }
        }
    })
}

module.exports = { loadCommands }