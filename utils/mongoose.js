const mongoose = require('mongoose')

module.exports = {
    init: async () => {
        const mongOptions = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
            autoIndex: false,
            poolSize: 10,
            serverSelectionTimeoutMS: 5000,
            socketTimeoutMS: 45000,
            family: 4
        }
        await mongoose.connect(process.env.DB_CONNECTION, mongOptions)
        mongoose.Promise = global.Promise
        mongoose.connection.on('connected', () => console.log('Mongoose connecté'))
    }
}