const Express = require('express')
const App = Express()
const Mongoose = require('mongoose')

module.exports = {
    init: () => {
        App.listen(process.env.PORT_SERVER, () => {
            console.log('Serveur à l\'écoute')
        })

        // Get all collections from database.
        App.get('/collections', async (req, res) => {
            let collections = []
            const data = await Mongoose.connection.db.collections()
            data.forEach((collection, index) => {
                collections.push(collection.namespace.split('.').pop())
            })
            res.send(collections)
        })

        // Get one collection from database.
        App.get('/:collection', async (req, res) => {
            const model = require(`../models/${ req.params.collection }`)
            const collection = model ? await model.find({}) : []
            res.send(collection)
        })

        // Get one item from a collection.
        App.get('/:collection/:id', async (req, res) => {
            const model = require(`../models/${ req.params.collection }`)
            const item = model ? await model.findById({ _id: req.params.id }) : []
            res.send(item)
        })

        /*App.get('/links/remove/:id', async (req,res) => {
            await Link.findByIdAndRemove({_id: req.params.id}, err => {
                if (err) {
                    res.send('error :' + err)
                }
                res.send('Link with id ' + req.params.id + ' deleted')
            })
        })*/
    }
}