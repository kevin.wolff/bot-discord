const MESSAGES = {
    COMMANDS: {
        INFOS: {
            LINKS: {
                DISPLAY: {
                    name: 'ldisplay',
                    description: 'Partager les liens',
                    arguments: 0,
                    usage: '',
                    role: '@everyone'
                },
                ADD: {
                    name: 'ladd',
                    description: 'Ajouter un lien',
                    arguments: 2,
                    usage: '<nom/emoji> <exemple.com>',
                    role: 'administrator'
                },
                REMOVE: {
                    name: 'lremove',
                    description: 'Supprimer un lien',
                    arguments: 1,
                    usage: '<numéro de la ligne à supprimer>',
                    role: 'administrator'
                }
            },
            SURVEYS: {
                ASK: {
                    name: 'sask',
                    description: 'Créer un sondage',
                    arguments: 0,
                    usage: '',
                    role: 'administrator'
                },
                HELP: {
                    name: 'shelp',
                    description: 'Aide au sondage',
                    arguments: 0,
                    usage: '',
                    role: 'administrator'
                }
            }
        }
    }
}

exports.MESSAGES = MESSAGES