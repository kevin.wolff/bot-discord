const Discord = require('discord.js')

// Analyse the message and execute the command if it's valid.
const handleCommands = async (client, message) => {
    if (!message.content.startsWith(process.env.PREFIX) || message.author.bot) return

    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/)
    const commandName = args.shift().toLowerCase()

    if (!client.commands.has(commandName)) return

    const command = client.commands.get(commandName)

    const authorRolesData = message.member.roles.cache
    const authorRoles = []
    authorRolesData.forEach((role) => {
        authorRoles.push(role.name)
    })

    if (!authorRoles.includes(command.info.role)) return;

    if (command.info.arguments !== args.length) {
        const errorEmbed = new Discord.MessageEmbed()
            .setTitle(`Commande érronée`)
            .setDescription(`Format attendu : \`${ process.env.PREFIX }${ command.info.name } ${ command.info.usage }\``)
        await message.channel.send(errorEmbed)
    } else {
        command.run(client, message, args)
    }
}

module.exports = { handleCommands }