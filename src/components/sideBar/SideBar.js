import { Link } from 'react-router-dom'
import Fetcher from '../../utils/Fetcher.js'
import './sideBar.scss'

export default function SideBar() {
    const [collections] = Fetcher('/collections')

    // Render collection's link.
    function NavCollections() {
        const navLink = collections.data.map((collection, index) =>
            <li key={ index }>
                <Link to={`/${ collection }`}>{ collection }</Link>
            </li>
        )

        return (
            <ul className="navCollection">
                { navLink }
            </ul>
        )
    }

    if (collections.loading) {
        return (
            <div className="sidebar">
                <p>Loading</p>
            </div>
        )
    }

    return (
        <div className="sidebar">
            <div className="brand">
                <img src="discord.png" alt="brand"/>
                <p>Bot React Admin</p>
            </div>
            <ul className="navbar">
                <li>
                    <Link to="/signup">Inscription</Link>
                </li>
                <li>
                    <Link to="/login">Connexion</Link>
                </li>
            </ul>
            <NavCollections/>
        </div>
    )
}