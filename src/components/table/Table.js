import { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import './table.scss'

/*
    @params props.rowData = Array
    @Render table.
 */
export function Table(props) {
    let [keys] = useState([])
    const history = useHistory()
    const collection = useParams().collection

    // Set keys array.
    if (props.rowData.length > 0) {
        keys = []
        const rawKeys = Object.keys(props.rowData[0])
        rawKeys.forEach(key => {
            keys.push(key)
        })
    }

    /*
        @params props.keysData = Array
        @Render table head.
    */
    function TableHead(props) {
        const keys = props.keysData.map((key, index) =>
            <th key={ index }>{ key }</th>
        )

        return (
            <thead>
            <tr>
                { keys }
            </tr>
            </thead>
        )
    }

    /*
        @params props.rowsData = Array
        @Render table body.
     */
    function TableBody(props) {
        const rows = props.rowsData.map((item, index) =>
            <TableRow key={ index } item={ item }/>
        )

        return (
            <tbody>
            { rows }
            </tbody>
        )
    }

    /*
        @params props.item = Object
        @Render table body rows.
     */
    function TableRow(props) {
        const values = Object.values(props.item)
        const data = values.map((value, index) =>
            <td key={ index }>{ value }</td>
        )

        return (
            <tr onClick={() => { handleRowClick(props.item._id) }}>
                { data }
            </tr>
        )
    }

    // Redirect to item's detail page.
    const handleRowClick = (id) => {
        history.push(`/${ collection }/detail/${ id }`);
    }

    return (
        <table className="content-table">
            <TableHead keysData={ keys }/>
            <TableBody rowsData={ props.rowData }/>
        </table>
    )
}

