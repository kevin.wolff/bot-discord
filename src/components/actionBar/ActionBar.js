import { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import './actionBar.scss'

/*
    @params props.view = String
    @Render actions bar.
 */
export function ActionBar(props) {
    const history = useHistory()
    const collection = useParams().collection
    const itemId = useParams().id

    function ButtonGenerator() {
        let [buttons] = useState([])

        const actions = [
            { name: 'new', trad: 'Créer', views:['collection'], function: actionNew },
            { name: 'edit', trad: 'Editer', views:['detail'], function: actionEdit },
            { name: 'remove', trad: 'Supprimer', views:['detail'], function: actionRemove },
            { name: 'save', trad: 'Sauvegarder', views:['save', 'quit'], function: actionSave },
            { name: 'quit', trad: 'Quitter sans sauvegarder', views:['save', 'quit'], function: actionQuit }
        ]

        actions.forEach((action, index) => {
            if (action.views.includes(props.view)) {
                buttons.push(
                    <div onClick={ action.function } className={ `actionBtn ${ action.name }` } key={ index }>
                        <p>{ action.trad }</p>
                    </div>
                )
            }
        })

        return (
            <div className="actions">
                { buttons }
            </div>
        )
    }

    function actionNew() {
        history.push(`/${ collection }/new`);
    }

    function actionEdit() {
        history.push(`/${ collection }/edit/${ itemId }`);
    }

    function actionRemove() {
        // btn remove -> supprimer l'item // ACTION
    }

    function actionSave() {
        // btn save -> sauvegarde l'item

        history.push(`/${ collection }`);
    }

    function actionQuit() {
        // btn quit -> ne sauvegarde pas

        history.push(`/${ collection }`);
    }

    return (
        <ButtonGenerator/>
    )
}