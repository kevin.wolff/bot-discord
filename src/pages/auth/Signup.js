import './auth.scss'

export default function Signup() {

    const handleSignup = event => {
        event.preventDefault()
        console.log('hello')
    }

    return (
        <div className="signupForm">
            <form onSubmit={ handleSignup }>
                <div>
                    <label htmlFor="email">
                        Adresse email
                        <input name="email" type="email" placeholder="Adresse email"/>
                    </label>
                </div>
                <div>
                    <label htmlFor="password">
                        Mot de passe
                        <input name="password" type="password" placeholder="Mot de passe"/>
                    </label>
                </div>
                <div>
                    <label htmlFor="confirmPassword">
                        Confirmer le mot de passe
                        <input name="confirmPassword" type="password" placeholder="Mot de passe"/>
                    </label>
                </div>
                <div>
                    <input type="submit"/>
                </div>
            </form>
        </div>
    )
}