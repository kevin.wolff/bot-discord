import './auth.scss'

export default function Login() {

    const handleLogin = event => {
        event.preventDefault()
        console.log('hello')
    }

    return (
        <div className="loginForm">
            <form onSubmit={ handleLogin }>
                <div>
                    <label htmlFor="email">
                        Adresse email
                        <input name="email" type="email" placeholder="adresse email"/>
                    </label>
                </div>
                <div>
                    <label htmlFor="password">
                        Mot de passe
                        <input name="password" type="password" placeholder="mot de passe"/>
                    </label>
                </div>
                <div>
                    <input type="submit"/>
                </div>
            </form>
        </div>
    )
}