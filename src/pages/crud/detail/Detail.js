import { useParams } from 'react-router-dom'
import Fetcher from '../../../utils/Fetcher.js'
import { ActionBar } from '../../../components/actionBar/ActionBar.js'
import './detail.scss'

export default function Detail() {
    const collection = useParams().collection
    const itemId = useParams().id
    const [item] = Fetcher(`/${collection}/${itemId}`)

    // console.log(collection)
    // console.log(itemId)
    // console.log(item)

    if (collection.loading) {
        return (
            <p>Loading</p>
        )
    }

    return (
        <div className="detail">
            <h1>Détail { collection } { itemId }</h1>
            <ActionBar view="detail"/>
        </div>
    )
}