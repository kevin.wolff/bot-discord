import { useParams } from 'react-router-dom'
import Fetcher from '../../utils/Fetcher.js'
import { ActionBar } from '../../components/actionBar/ActionBar.js'
import { Table } from '../../components/table/Table.js'
import './collection.scss'

export default function Collection() {
    const uri = useParams().collection
    const [collection] = Fetcher(uri)

    if (collection.loading) {
        return (
            <p>Loading</p>
        )
    }

    return (
        <div className="collection">
            <h1>Collection { uri }</h1>
            <ActionBar view="collection"/>
            <Table rowData={ collection.data }/>
        </div>
    )
}