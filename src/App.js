import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import SideBar from './components/sideBar/SideBar.js'
import Signup from './pages/auth/Signup.js'
import Login from './pages/auth/Login.js'
import Collection from './pages/collection/Collection.js'
import Detail from './pages/crud/detail/Detail.js'
import Edit from './pages/crud/edit/Edit.js'
import New from './pages/crud/new/New.js'
import './app.scss'

function App() {

    return (
        <React.Fragment>
            <Router>
                <div className="layout">
                    <SideBar/>
                    <Switch>
                        <Route exact path="/signup" component={ Signup }/>
                        <Route exact path="/login" component={ Login }/>
                        <Route exact path="/:collection" component={ Collection }/>
                        <Route exact path="/:collection/detail/:id" component={ Detail }/>
                        <Route exact path="/:collection/edit/:id" component={ Edit }/>
                        <Route exact path="/:collection/new" component={ New }/>
                    </Switch>
                </div>
            </Router>
        </React.Fragment>
    )
}

export default App
