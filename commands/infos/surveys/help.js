const Discord = require('discord.js')
const { resolve } = require('path')
const { MESSAGES } = require(resolve('./utils/constant.js'))

module.exports.info = MESSAGES.COMMANDS.INFOS.SURVEYS.HELP

//
module.exports.run = (client, message, args) => {
    const steps = [
        `Etape 1 : renseigner la question de l'enquête \`<votre question>\``,
        `Etape 2 : renseigner la durée de l'enquête \`<durée en seconde min 15 max 600>\``,
        `Etape 3 : renseigner le nombre de réponses possible \`<min 2 max 4>\``,
        `Etape 4 : renseigner une réponse pour  chaque possibilité \`<votre réponse>\``,
        ` `,
        `🎉 Votre enquête devrait commencer !`,
        `⚠ Impossible d'utiliser des emojis dans les questions et réponses`
    ]

    let formatedMessage = ''
    steps.forEach(step => {
        formatedMessage = formatedMessage + `${step}\n`
    })

    let helpEmbed = new Discord.MessageEmbed()
        .setTitle('Aide à la création d\'une enquête')
        .setDescription(formatedMessage)
    message.channel.send(helpEmbed)
}