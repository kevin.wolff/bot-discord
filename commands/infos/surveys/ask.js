const Discord = require('discord.js')
const { resolve } = require('path')
const { MESSAGES } = require(resolve('./utils/constant.js'))

module.exports.info = MESSAGES.COMMANDS.INFOS.SURVEYS.ASK

// Interactive dialog for create a survey.
module.exports.run = async (client, message, args) => {
    const surveyAuthor = message.author.id
    let survey = {}
    const reactions = ["🍉", "🍎", "🍍", "🥝", "🥥"]
    const textFilter = (message) => !message.author.bot && message.author.id === surveyAuthor && /^[a-zA-Z0-9?éèêîïôùüûç'" ]*$/.test(message)
    const numberFilter = (message) => !message.author.bot && message.author.id === surveyAuthor && /^[0-9]*$/.test(message)
    const reactionFilter = (reaction, user) => reactions.includes(reaction.emoji.name) && !user.bot
    const options = { max: 1, time: 15000 }
    const errorMsg = `Format érroné ou temps écoulé, voir \`!shelp\``

    // Ask for the survey question.
    await message.channel.send('Que souhaitez vous demander ?')
    const surveyQuestion = await message.channel.awaitMessages(textFilter, options)
    if (surveyQuestion.size > 0) {
        survey.question = surveyQuestion.array()[0].content
    } else {
        return message.channel.send(errorMsg)
    }

    // Ask for the survey duration.
    await message.channel.send('Combien de secondes l\'enquête doit-elle durer ? min 15 max 600')
    const surveyDuration = await message.channel.awaitMessages(numberFilter, options)
    if (surveyDuration.size > 0) {
        const inputDuration = parseInt(surveyDuration.array()[0].content)
        if (inputDuration >= 15 && inputDuration <= 600) {
            survey.duration = inputDuration * 1000
        } else {
            return message.channel.send(errorMsg)
        }
    } else {
        return message.channel.send(errorMsg)
    }

    // Ask for the number of possibilities.
    await message.channel.send('Combien de réponses possibles ? min 2 max 4')
    const surveyAnswersLength = await message.channel.awaitMessages(numberFilter, options)
    if (surveyAnswersLength.size > 0) {
        const inputNumber = parseInt(surveyAnswersLength.array()[0].content)
        if (inputNumber >= 2 && inputNumber <= 4) {
            survey.answersLength = inputNumber
            survey.answers = []
        } else {
            return message.channel.send(errorMsg)
        }
    } else {
        return message.channel.send(errorMsg)
    }

    // For each possibilities ask an answer.
    for (let i = 0; i < survey.answersLength; i++) {
        await message.channel.send(`Quelle est la réponse numéro ${i}`)
        const surveyAnswer = await message.channel.awaitMessages(textFilter, options)
        if (surveyAnswer.size > 0) {
            survey.answers.push(surveyAnswer.array()[0].content)
        }
    }

    // Build the survey embed message, then create the reactions.
    survey.description = ''
    survey.answers.forEach((answer, index) => {
        survey.description += `${reactions[index]} ${answer}\n`
    })
    const surveyEmbed = new Discord.MessageEmbed()
        .setTitle(survey.question)
        .setDescription(survey.description)
    const embedMessage = await message.channel.send(surveyEmbed)
    for (let i = 0; i < survey.answersLength; i++) {
        await embedMessage.react(reactions[i])
    }

    // Wait for reactions, then create an object containing collected reactions.
    const surveyOptions = {time: survey.duration}
    const surveyRawResults = await embedMessage.awaitReactions(reactionFilter, surveyOptions)
    const collectedArray = surveyRawResults.array()
    const collectedReactions = collectedArray.map(reaction => reaction._emoji.name)
    let reactionCounts = {}
    collectedReactions.forEach(reaction => {
        if (reactionCounts[reaction]) {
            reactionCounts[reaction]++
        } else {
            reactionCounts[reaction] = 1
        }
    })

    // Build the survey results embed message.
    let surveyResults = ''
    survey.answers.forEach((answer, index) => {
        let voteCount = 0
        if (reactionCounts[reactions[index]]) {
            voteCount = reactionCounts[reactions[index]]
        }
        surveyResults += `${reactions[index]} ${answer} (${voteCount} vote${voteCount !== 1 ? 's' : ''})\n`
    })
    const surveyResultsEmbed = new Discord.MessageEmbed()
        .setTitle(`${survey.question} (${collectedArray.length} votes)`)
        .setDescription(surveyResults)
    await message.channel.send(surveyResultsEmbed)
}