const Discord = require('discord.js')
const { resolve } = require('path')
const { MESSAGES } = require(resolve('./utils/constant.js'))
const Link = require(resolve('./models/links.js'))

module.exports.info = MESSAGES.COMMANDS.INFOS.LINKS.DISPLAY

// Return links from database.
module.exports.run = async (client, message, args) => {
    const links = await Link.find({}, err => {
        if (err) message.channel.send('Problème lors du chargement des liens')
    })

    if (links.length > 0) {
        let formatedMessage = ''
        for (const link of links) {
            formatedMessage = formatedMessage + ' \n' + link.name + ' <' + link.url + '>'
        }

        const linksEmbed = new Discord.MessageEmbed()
            .setTitle('Liste des liens sauvegardés')
            .setDescription(formatedMessage)
        await message.channel.send(linksEmbed)
    } else {
        await message.channel.send('Aucun lien à partager')
    }
}