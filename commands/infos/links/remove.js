const { resolve } = require('path')
const { MESSAGES } = require(resolve('./utils/constant.js'))
const Link = require(resolve('./models/links.js'))

module.exports.info = MESSAGES.COMMANDS.INFOS.LINKS.REMOVE

// Remove links from database.
module.exports.run = async (client, message, args) => {
    const links = await Link.find({})

    if (links.length > 0) {
        if (args[0] <= links.length && args[0] >= 0) {
            const indexLinkToRemove = args[0] - 1

            await Link.findByIdAndRemove({_id: links[indexLinkToRemove]._id}, err => {
                message.channel.send('Lien supprimé !')
                if (err) {
                    message.channel.send('Problème lors de la suppression du lien')
                }

            })
        } else {
            await message.channel.send('La ligne à supprimer n\'existe pas')
        }
    } else {
        await message.channel.send('Aucun lien à supprimer')
    }
}