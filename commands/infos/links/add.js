const { resolve } = require('path')
const { MESSAGES } = require(resolve('./utils/constant.js'))
const Link = require(resolve('./models/links.js'))

module.exports.info = MESSAGES.COMMANDS.INFOS.LINKS.ADD

// Add a link to the database.
module.exports.run = async (client, message, args) => {
    const newLink = new Link({
        name: args[0],
        url: 'https://' + args[1]
    })
    await newLink.save(err => {
        message.channel.send('Lien ajouté !')
        if (err) {
            message.channel.send('Probleme lors de l\'ajout du lien')
        }
    })
}