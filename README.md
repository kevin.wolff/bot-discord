# Bot discord

Réalisé avec Node.js, j'agrémente ce bot de temps en temps ! work in progress

L'idée est d'avoir un bot Discord permettant l'administration d'un serveur, administrable depuis le chat et une application web.

**Feature présente :**

- Consulter une liste de liens utiles + créer/supprimer un lien
- Créer une enquête + affichage des résultats
- Gestion des permissions pour l'utilisation des commandes
- Aide lors de l'utilisation des commandes

## Initialiser le bot

Comment créer un bot Discord, [documentation](https://www.digitalocean.com/community/tutorials/how-to-build-a-discord-bot-with-node-js-fr).

```shell
npm install
```

Configurer votre fichier .env

```shell
cp .env.dist .env
```

Lancer le serveur + hot reload du serveur

```shell
node index.js
nodemon index.js
```

Lancer l'application web

```shell
npm run start
```

## BDD - MongoDB et Mongoose

Le fichier de configuration de Mongoose se trouve dans `./utils/mongoose.js`.

Exemple d'utilisation de la base de données :

- un administrateur ajoute un "lien utile" depuis Discord grâce à la commande `!ladd google google.com` le nom du lien "google" et son url "google.com" sont stockés en base de données.
- lorsqu'un utilisateur utilise la commande `!ldisplay` depuis Discord le serveur intérroge la base de donnée pour retourner les liens stockés.

## API - Express

Le fichier de configuration de Express se trouve dans `./utils/express.js`.

L'API rend disponible le contenu de la base de donnée pour l'application web React.

## Web - React

L'application Web React permet à un administrateur de gérer le contenu de la base de donnée (liens, exclusion, etc) via une interface 

## Utilisation

### Créer une commande

Il est indispensable de créer ses commandes dans le dossier `./commands`, en respéctant cette structure :

`./commands/<categorie>/<nom de la commande>/<fichier>.js`

Déclarer sa commande dans le fichier `constant.js` de la manière suivante :

```javascript
const MESSAGES = {
    COMMANDS: {
        <categorie>: {
            <nom de la commande>: {
                <fichier.js>: {
                    name: '<command à éxecuter dans un channel>',
                    description: '<description>',
                    arguments: '<nombre d argument(s)>',
                    usage: '<format attendu, affiché sous forme d aide>',
                    role: '<role minimum pour utiliser la commande>'
                },
```

Exemple.

```javascript
const MESSAGES = {
    COMMANDS: {
        INFOS: {
            LINKS: {
                ADD: {
                    name: 'ladd',
                    description: 'Ajouter un lien',
                    arguments: 2,
                    usage: '<nom/emoji> <exemple.com>',
                    role: 'administrator'
                },
```

le fichier de la commande doit respecter cette structure :

```javascript
const { resolve } = require('path')
const { MESSAGES } = require(resolve('./utils/constant.js'))

module.exports.info = MESSAGES.COMMANDS.INFOS.<CATEGORIE>.<NOM COMMANDE>
// module.exports.info = MESSAGES.COMMANDS.INFOS.LINKS.ADD

module.exports.run = async (client, message, args) => {
    // votre code ...
    // message.channel.send('Hello world')
}
```

Vous devriez voir le nom de la commande apparaitre dans votre terminal au démarrage de votre serveur.
